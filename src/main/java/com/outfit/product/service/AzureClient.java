package com.outfit.product.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.outfit.product.ApiException;
import com.outfit.product.model.OrderedProduct;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class AzureClient {
    private static final String GET_PRODUCTS_REQUEST_URL = "https://op-app.azurewebsites.net/api/dev-test/get-products";
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Value("${api.azure.key}")
    private String apiKey;
    @Value("${api.azure.cookie}")
    private String cookie;

    public List<OrderedProduct> getOrderedProducts() {
        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Request request = new Request.Builder()
                .url(GET_PRODUCTS_REQUEST_URL)
                .method("GET", null)
                .addHeader("API_KEY", apiKey)
                .addHeader("Cookie", cookie)
                .build();

        try {
            ResponseBody body = client.newCall(request).execute().body();
            return toOrderedProduct(body.string());
        } catch (IOException e) {
            throw new ApiException("Could not get product information. Cause:", e);
        }
    }

    private List<OrderedProduct> toOrderedProduct(String json) {
        try {
            return objectMapper.readValue(json, new TypeReference<>() {});
        } catch (IOException e) {
            return List.of();
        }
    }
}
