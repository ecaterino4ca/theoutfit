package com.outfit.product.service;

import com.outfit.product.ApiException;
import com.outfit.product.model.OrderedProduct;
import com.outfit.product.model.OrderedProductStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderedProductService {
    private final AzureClient azureClient;

    @Autowired
    public OrderedProductService(AzureClient azureClient) {
        this.azureClient = azureClient;
    }

    public Double getAverageNumberOfProductsSoldPerOrder() {
        return Optional.ofNullable(azureClient.getOrderedProducts())
                .orElse(new ArrayList<>())
                .stream()
                .collect(Collectors.groupingBy(OrderedProduct::getOrderId))
                .values()
                .stream()
                .mapToLong(this::getNumberOfSoldProducts)
                .average()
                .orElseThrow(() -> new ApiException("Not enough data to get the average products sold"));
    }

    public List<Long> getMostPopularProducts(Integer limit) {
        Map<Long, Long> productIdToPurchaseProbability = Optional.ofNullable(azureClient.getOrderedProducts())
                .orElse(new ArrayList<>())
                .stream()
                .collect(Collectors.groupingBy(OrderedProduct::getProductId))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> getPurchaseProbability(entry.getValue())));

        return getTopEntries(productIdToPurchaseProbability, limit);
    }

    public List<String> getMostPopularBrands(Integer limit) {
        Map<String, Long> brandToPurchaseProbability = Optional.ofNullable(azureClient.getOrderedProducts())
                .orElse(new ArrayList<>())
                .stream()
                .collect(Collectors.groupingBy(OrderedProduct::getBrand))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> getPurchaseProbability(entry.getValue())));

        return getTopEntries(brandToPurchaseProbability, limit);
    }

    private long getNumberOfSoldProducts(List<OrderedProduct> products) {
        return products.stream()
                .filter(product -> OrderedProductStatus.VANDUT.equals(product.getStatus()))
                .count();
    }

    private <T> List<T> getTopEntries(Map<T, Long> itemToPurchaseProbability, Integer limit) {
        return itemToPurchaseProbability
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .limit(limit)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private long getPurchaseProbability(List<OrderedProduct> products) {
        if (CollectionUtils.isEmpty(products)) {
            return 0;
        }

        return getNumberOfSoldProducts(products) / products.size();
    }

}
