package com.outfit.product;

public class ApiException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ApiException(Throwable cause) {
        super(cause);
    }

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

}
