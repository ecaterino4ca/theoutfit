package com.outfit.product.controller;

import com.outfit.product.service.OrderedProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {
    public static final int DEFAULT_LIMIT = 10;
    private final OrderedProductService productService;

    @Autowired
    public ProductController(OrderedProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/averageSold")
    @ResponseBody
    public Double getAverageNumberOfProductsSoldPerOrder() {
        return productService.getAverageNumberOfProductsSoldPerOrder();
    }

    @GetMapping("/product/mostPopular")
    @ResponseBody
    public List<Long> getMostPopularProducts(@RequestParam Optional<Integer> limit) {
        return productService.getMostPopularProducts(getLimit(limit));
    }

    @GetMapping("/brand/mostPopular")
    @ResponseBody
    public List<String> getMostPopularBrands(@RequestParam Optional<Integer> limit) {
        return productService.getMostPopularBrands(getLimit(limit));
    }

    private int getLimit(Optional<Integer> limit) {
        int productsLimit = limit.orElse(DEFAULT_LIMIT);
        productsLimit = productsLimit > 0 ? productsLimit : DEFAULT_LIMIT;
        return productsLimit;
    }
}
