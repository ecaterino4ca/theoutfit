package com.outfit.product.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class OrderedProduct {
    private String orderId;
    private Long productId;
    private String size;
    private OrderedProductStatus status;
    private String brand;
}
