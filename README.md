**How do we measure a product's performance? When we know that the stock should be updated?**

To find a product's performance we could take into account multiple variables such as:

 * What is the rate of purchase for this product? `Rate = Nr of times it was purchased/ Nr of times it appeared in the order`.
 * How likable is this product overall? This would be based on the rating we receive from customers. Let's say we translate it into a scale from 0 to 1, 1 meaning it's extremely popular/likable.

Performance might be something like: `0.7 * rate of purchase + 0.3 * likability rate`

Other things that could give us a sign what is more trendy for our customers:

 * What is the purchase ratio for a specific brand? 
 * What is the purchase ratio for a specific type of clothing? 

Things we need to take into account to fill in the stock: 

 * **Current stock**: What is the current stock for this product
 * **Feedback**: Did we receive feedback that customers wanted a different size (bigger/smaller)? If so this should be taken into account when ordering more items from the vendor because it could show that the items size isn't aligned. 
 Ex: we have a very likable dress, that most women with size M considered too tight. That'll probably mean that we need to order more such dresses but of size L. Also we should ether suggest or at least notify stylists that this dress is actually smaller than what is labeled.
 * **Trending**: Is this type of clothing trendy? Ex: customers tend to buy more high waisted pants means we need to supply more of such clothing
 * **Seasonality**: If it's end of year, probably no need to buy satin summer dresses, better focus on warmer fabrics. 
 Ex: festive dresses will have a higher probability around Christmas & New Year. Long sleeve clothing has more probability to be purchased during Oct-April.
 * **Brand recognition**: Does this brand have popularity amongst our customers? If so, we need to look into other clothing and accessories they offer.
 * **Clothing typology**: What type of clothing this is? Is it a basic element of the wardrobe or something for more special occasions? Usually first types need to be supplied in a higher amount. 
 Similar is with colors: white/black/beige are more popular than red/green/yellow. Here could come in handy a colour preference matrix or a way to measure what colour have more popularity.
 
 **Style lookalikes. How we do define the clusters?**
 
 Some ideas of what clusters we could consider:
 
 - office
 - comfy
 - feminine
 - maternity
 - top fashion (includes the latest trends)
 - teenager
 - middle-age
 - sports
 - casual
 - classy/timeless
 
 It does not mean that a person should fit into only one cluster. 
 A set of 5 outfits could include examples from different clusters: like 1 for home, 2 for office, 1 for a date, 1 for sports.
 
 We could ask our customers what is the purpose of the outfit: for home, office, a date. 
 Depending on the things they chose on the previous question, we could follow-up asking for more details on how they want to feel: romantic, confident, approachable, comfy etc.
 
 Also depending on the prevalence of one cluster or the other in a users' response, we could ask follow-up questions to help define sub-clusters. 
 Let's say for sports that might be: indoor, outdoor, winter, aqua etc.
  